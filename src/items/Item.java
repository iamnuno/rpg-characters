package items;

/**
 * Abstract class from where armor and weapon classes inherit.
 */
public abstract class Item {

    protected String name;
    protected int level;


    public Item(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
