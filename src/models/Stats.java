package models;

/**
 * Class to hold different stats values.
 */
public class Stats {
    private int hp;
    private int strength;
    private int dexterity;
    private int intelligence;

    public Stats(int hp, int strength, int dexterity, int intelligence) {
        this.hp = hp;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    @Override
    public String toString() {
        return "HP: " + hp +
                "\nStr: " + strength +
                "\nDex: " + dexterity +
                "\nInt: " + intelligence;
    }
}
