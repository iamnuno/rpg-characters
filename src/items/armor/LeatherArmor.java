package items.armor;

import models.Stats;

/**
 * Type of armor with own specific stats.
 */
public class LeatherArmor implements ArmorStyle {

    /**
     * Leather armor has its own stats and level is used to scaled them.
     *
     * @param level int used to update stats value
     * @return stats object with calculated stats
     */
    @Override
    public Stats updateBonusStats(int level) {
        return new Stats(20 + (8 * level),1 + level, 3 + (2 * level), 0);
    }

    @Override
    public String getType() {
        return "Leather";
    }
}
