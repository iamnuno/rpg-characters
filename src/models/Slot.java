package models;

/**
 * Used for items to keep track of which slot thy were added on to.
 * The type of slot with influence its stats scaling (armors).
 */
public class Slot {
    private SlotType slotType;

    public Slot(SlotType slotType) {
        this.slotType = slotType;
    }

    public SlotType getSlotType() {
        return slotType;
    }

    public void setSlotType(SlotType slotType) {
        this.slotType = slotType;
    }

}
