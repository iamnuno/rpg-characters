package items.weapons;

/**
 * Type of weapon with specific own damage, scaled with item level.
 */
public class MagicWeapon implements WeaponStyle {

    @Override
    public int updateBaseDamage(int level) {
        return 25 + (level * 2);
    }

    @Override
    public String getType() {
        return "Magic";
    }

}
