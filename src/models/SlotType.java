package models;

public enum SlotType {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
