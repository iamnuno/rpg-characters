# RPG Characters

Java console game designed to test modelling and class design skills.

There is no real game logic, however a series of simulations can be run to test the implementation:
* [1] Create characters (heroes) and level them up based on experience points 
* [2] Create items (weapons and armor), show their stats scaling according to their type and level
* [3] Equip characters with items and realize their stats are updated accordingly with the items' bonus stats and slot used
* [4] Attack with a hero and display attack damage calculated based on type of weapon and the hero's stats

## Functionality demonstration

Below you can see the simulation for each of the above mention functionalities.

### [1]

![](resources/heroes.gif)

### [2]

![](resources/items.gif)

### [3]

![](resources/equip.gif)

### [4]

![](resources/attack.gif)
