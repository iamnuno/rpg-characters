package items.weapons;

/**
 * Type of weapon with specific own damage, scaled with item level.
 */
public class MeleeWeapon implements WeaponStyle {

    @Override
    public int updateBaseDamage(int level) {
        return 15 + (level * 2);
    }

    @Override
    public String getType() {
        return "Melee";
    }
}
