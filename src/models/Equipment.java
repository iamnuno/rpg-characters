package models;

import items.armor.Armor;
import items.weapons.Weapon;

/**
 * Class holding items which will be equipped by heroes (characters).
 */
public class Equipment {
    private Weapon weapon;
    private Armor head;
    private Armor body;
    private Armor legs;

    public Equipment(Weapon weapon, Armor head, Armor body, Armor legs) {
        this.weapon = weapon;
        this.head = head;
        this.body = body;
        this.legs = legs;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Armor getHead() {
        return head;
    }

    public void setHead(Armor head) {
        this.head = head;
    }

    public Armor getBody() {
        return body;
    }

    public void setBody(Armor body) {
        this.body = body;
    }

    public void setLegs(Armor legs) {
        this.legs = legs;
    }

    public Armor getLegs() {
        return legs;
    }
}
