package heroes;

import models.Level;
import models.Stats;

/**
 * Child class of Hero.
 * Has its own base stats and level up.
 */
public class MageHero extends Hero {

    public MageHero() {
        super(new Stats(100,2,3,10),
                new Level(1, 0, 100));
    }

    /**
     * Leveling up scales on number of level ups.
     *
     * @param levelUpCount int representing how many levels the character upgraded
     */
    @Override
    public void levelUp(int levelUpCount) {
        for (int i = 0; i < levelUpCount; i++) {
            stats.setHp(stats.getHp() + 15);
            stats.setStrength(stats.getStrength() + 1);
            stats.setDexterity(stats.getDexterity() + 2);
            stats.setIntelligence(stats.getIntelligence() + 5);
        }
    }
}
