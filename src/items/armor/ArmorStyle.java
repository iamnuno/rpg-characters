package items.armor;

import models.Stats;

/**
 * Each type of armor has its own bonus stats, hence needing its own implementation
 * to update them.
 */
public interface ArmorStyle {
    Stats updateBonusStats(int level);
    String getType();
}

