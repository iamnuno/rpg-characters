package heroes;

import models.Level;
import models.Stats;

/**
 * Child class of Hero.
 * Has its own base stats and level up.
 */
public class RangerHero extends Hero {

    public RangerHero() {
        super(new Stats(120,5,10,2),
                new Level(1, 0, 100));
    }

    /**
     * Leveling up scales on number of level ups.
     *
     * @param levelUpCount int representing how many levels the character upgraded
     */
    @Override
    public void levelUp(int levelUpCount) {
        for (int i = 0; i < levelUpCount; i++) {
            stats.setHp(stats.getHp() + 20);
            stats.setStrength(stats.getStrength() + 2);
            stats.setDexterity(stats.getDexterity() + 5);
            stats.setIntelligence(stats.getIntelligence() + 1);
        }
    }
}
