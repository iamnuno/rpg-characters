package items.weapons;

/**
 * Factory returns specified weapon style object.
 */
public class WeaponStyleFactory {
    public WeaponStyle getWeaponStyle(WeaponType weaponType) {
        return switch (weaponType) {
            case MAGIC -> new MagicWeapon();
            case MELEE -> new MeleeWeapon();
            case RANGE -> new RangeWeapon();
        };
    }
}
