import heroes.*;
import items.armor.Armor;
import items.armor.ArmorStyleFactory;
import items.armor.ArmorType;
import items.weapons.Weapon;
import items.weapons.WeaponStyleFactory;
import items.weapons.WeaponType;
import models.Equipment;
import models.Slot;
import models.SlotType;

import java.util.Scanner;

/**
 * Prints game menu with options to simulate the game
 */
public class Game {
    public static void mainMenu(Scanner input) {
        String option;
        do {
            System.out.println();
            System.out.println("####### RPG Character #######");
            System.out.println();
            System.out.println("[1] Level up");
            System.out.println("[2] Item creation");
            System.out.println("[3] Equip heroes");
            System.out.println("[4] Attack!");
            System.out.println("[Q] Quit");
            System.out.println();
            System.out.print("Enter option > ");

            option = input.nextLine().toLowerCase().trim();
            switch (option) {
                case "1" -> levelUpSimulation();
                case "2" -> itemCreationSimulation();
                case "3" -> equipHeroSimulation();
                case "4" -> attackSimulation();
                case "q" -> System.out.println("Bye!");
                default -> System.out.println("Invalid option.");
            }
        } while (!option.equals("q"));
    }

    private static void levelUpSimulation() {
        HeroFactory heroFactory = new HeroFactory();

        // Warrior level up simulation
        Hero warrior = heroFactory.getHero(HeroTypes.WARRIOR);
        System.out.println("\nWarrior created!");
        System.out.println("\nWarrior details:");
        warrior.display();
        System.out.println("\nIncreasing warrior's XP by 1200...");
        warrior.increaseXp(1200);
        System.out.println("\nWarrior details:");
        System.out.println();
        warrior.display();

        // Mage level up simulation
        Hero mage = heroFactory.getHero(HeroTypes.MAGE);
        System.out.println("\nMage created!");
        System.out.println("\nMage details:");
        mage.display();
        System.out.println("\nIncreasing mage's XP by 1500...");
        mage.increaseXp(1500);
        System.out.println("\nMage details:");
        System.out.println();
        mage.display();

        // Ranger level up simulation
        Hero ranger = heroFactory.getHero(HeroTypes.RANGER);
        System.out.println("\nRanger created!");
        System.out.println("\nRanger details:");
        ranger.display();
        System.out.println("\nIncreasing ranger's XP by 1900...");
        ranger.increaseXp(1900);
        System.out.println("\nRanger details:");
        System.out.println();
        ranger.display();
    }

    private static void itemCreationSimulation() {
        WeaponStyleFactory weaponStyleFactory = new WeaponStyleFactory();

        // Range Weapon
        Slot weaponSlot = new Slot(SlotType.WEAPON);
        Weapon weapon =
                new Weapon("The Bow and Arrow of Doom",10, weaponStyleFactory.getWeaponStyle(WeaponType.RANGE), weaponSlot);
        weapon.updateBaseDamage();
        weapon.display();

        // Melee Weapon
        weapon.setWeaponStyle(weaponStyleFactory.getWeaponStyle(WeaponType.MELEE));
        weapon.setName("The Sword of Doom");
        weapon.setLevel(1);
        weapon.updateBaseDamage();
        weapon.display();

        // Magic Weapon
        weapon.setWeaponStyle(weaponStyleFactory.getWeaponStyle(WeaponType.MAGIC));
        weapon.setName("The Spell of Doom");
        weapon.setLevel(15);
        weapon.updateBaseDamage();
        weapon.display();

        // Cloth Armor
        ArmorStyleFactory armorStyleFactory = new ArmorStyleFactory();
        Slot legs = new Slot(SlotType.LEGS);
        Armor armor =
                new Armor("Super Mega Cloth", 10, armorStyleFactory.getArmorStyle(ArmorType.CLOTH), legs);
        armor.updateBonusStats();
        armor.scaleBonusStats();
        armor.display();

        // Plate Armor
        armor.setArmorStyle(armorStyleFactory.getArmorStyle(ArmorType.PLATE));
        armor.setName("Plate of Steel");
        Slot body = new Slot(SlotType.BODY);
        armor.setSlot(body);
        armor.setLevel(15);
        armor.updateBonusStats();
        armor.scaleBonusStats();
        armor.display();

        // Leather Armor
        armor.setArmorStyle(armorStyleFactory.getArmorStyle(ArmorType.LEATHER));
        armor.setName("Unicorn Leather");
        Slot head = new Slot(SlotType.HEAD);
        armor.setSlot(head);
        armor.setLevel(5);
        armor.updateBonusStats();
        armor.scaleBonusStats();
        armor.display();
    }

    private static void equipHeroSimulation() {
        // Create hero
        HeroFactory heroFactory = new HeroFactory();
        Hero hero = heroFactory.getHero(HeroTypes.WARRIOR);
        hero.increaseXp(1200);
        System.out.println("\nWarrior details:");
        System.out.println();
        hero.display();

        // Create armors
        ArmorStyleFactory armorStyleFactory = new ArmorStyleFactory();
        Slot body = new Slot(SlotType.BODY);
        Armor armorBody =
                new Armor("Plate of Doom", 5, armorStyleFactory.getArmorStyle(ArmorType.PLATE), body);
        armorBody.updateBonusStats();
        armorBody.scaleBonusStats();
        armorBody.display();

        Slot legs = new Slot(SlotType.LEGS);
        Armor armorLegs =
                new Armor("Plate of Mega Doom", 205, armorStyleFactory.getArmorStyle(ArmorType.LEATHER),legs);
        armorLegs.updateBonusStats();
        armorLegs.scaleBonusStats();
        armorLegs.display();

        // Equipping hero
        System.out.println("\nEquipping hero with items...");
        Equipment equipment = new Equipment(null, null, armorBody, armorLegs);
        hero.equip(equipment);
        System.out.println("\nWarrior details:\n");
        hero.display();

        // Changing equipment
        System.out.println("\nReplacing armor...");
        armorBody.setName("Plate of Joy");
        armorBody.setLevel(2);
        armorBody.updateBonusStats();
        armorBody.scaleBonusStats();
        armorBody.display();

        equipment.setBody(armorBody);
        hero.unEquip();
        hero.equip(equipment);
        System.out.println("\nWarrior details:\n");
        hero.display();
    }

    public static void attackSimulation() {
        // Create hero
        HeroFactory heroFactory = new HeroFactory();
        Hero hero = heroFactory.getHero(HeroTypes.WARRIOR);
        hero.increaseXp(1200);
        System.out.println("\nWarrior details:");
        System.out.println();
        hero.display();

        // Create weapon
        WeaponStyleFactory weaponStyleFactory = new WeaponStyleFactory();
        Slot weaponSlot = new Slot(SlotType.WEAPON);
        Weapon weapon =
                new Weapon("The Sword of Doom",5, weaponStyleFactory.getWeaponStyle(WeaponType.MELEE), weaponSlot);
        weapon.updateBaseDamage();
        weapon.display();

        // Create armor
        ArmorStyleFactory armorStyleFactory = new ArmorStyleFactory();
        Slot body = new Slot(SlotType.BODY);
        Armor armor =
                new Armor("Plate of Steel", 5, armorStyleFactory.getArmorStyle(ArmorType.PLATE), body);
        armor.updateBonusStats();
        armor.scaleBonusStats();
        armor.display();

        // Equip hero
        System.out.println("\nEquipping hero with items...");
        Equipment equipment = new Equipment(weapon, null, armor, null);
        hero.equip(equipment);
        System.out.println("\nWarrior details:\n");
        hero.display();

        // Attack
        System.out.println("\nHero attacking...");
        hero.attack();
    }
}
