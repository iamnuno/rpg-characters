package items.weapons;

/**
 * Each weapon style has its own damage and level is used for scaling.
 */
public interface WeaponStyle {
    int updateBaseDamage(int level);
    String getType();
}
