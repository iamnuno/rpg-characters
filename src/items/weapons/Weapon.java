package items.weapons;

import items.Item;
import models.Slot;

/**
 * Item's child class.
 * Each weapon has its own style.
 */
public class Weapon extends Item {

    private int baseDamage = 0;
    private WeaponStyle weaponStyle;
    private Slot slot;

    public Weapon(String name, int level, WeaponStyle weaponStyle, Slot slot) {
        super(name, level);
        this.weaponStyle = weaponStyle;
        this.slot = slot;
    }

    /**
     * Weapon's damage is updated based on weapon's own style.
     * Level is used for scaling.
     */
    public void updateBaseDamage() {
        this.baseDamage = weaponStyle.updateBaseDamage(level);
    }

    public String getWeaponType() {
        return weaponStyle.getType();
    }

    public void display() {
        System.out.println("\nStats for item: " + name + "\n");
        System.out.println("Type: " + getWeaponType() + " Weapon");
        System.out.println("Level: " + level);
        System.out.println("Damage: " +  baseDamage);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBaseDamage() {
        return baseDamage;
    }

    public void setBaseDamage(int baseDamage) {
        this.baseDamage = baseDamage;
    }

    public WeaponStyle getWeaponStyle() {
        return weaponStyle;
    }

    public void setWeaponStyle(WeaponStyle weaponStyle) {
        this.weaponStyle = weaponStyle;
    }

    public int getLevel() {
        return super.getLevel();
    }

    public void setLevel(int level) {
        super.setLevel(level);
    }


}
