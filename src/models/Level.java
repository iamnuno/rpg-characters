package models;

/**
 * Class used by heroes to keep track of their level, xp and necessary xp to level up.
 */
public class Level {
    private int level;
    private int xp;
    private int aimXp;

    public Level(int level, int xp, int aimXp) {
        this.level = level;
        this.xp = xp;
        this.aimXp = aimXp;
    }

    /**
     * Increase xp based on xp int passed as argument.
     * When increasing xp check if level needs to be updated, if so update it and track how many
     * level ups occurred so it can be returned as response. Heroes will needs their base stats to be scaled based
     * on their level.
     *
     * @param xp integer holding xp to increase
     * @return number of level ups occurred
     */
    public int increaseXp(int xp) {
        this.xp += xp;
        int levelup = 0;
        while (this.xp >= aimXp) {
            level++;
            this.xp -= aimXp;
            if (this.xp < 0) {
                this.xp = 0;
            }
            aimXp *= 1.1;
            levelup++;
        }
        return levelup;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Level: " + level +
                "\nXP: " + xp +
                "\nXP to level up (total): " + aimXp;
    }
}
