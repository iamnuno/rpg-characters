package heroes;

import models.Level;
import models.Stats;

/**
 * Child class of Hero.
 * Has its own base stats and level up.
 */
public class WarriorHero extends Hero {

    public WarriorHero() {
        super(new Stats(150,10,3,1),
                new Level(1, 0, 100));
    }

    /**
     * Leveling up scales on number of level ups.
     *
     * @param levelUpCount int representing how many levels the character upgraded
     */
    @Override
    public void levelUp(int levelUpCount) {
        for (int i = 0; i < levelUpCount; i++) {
            stats.setHp(stats.getHp() + 30);
            stats.setStrength(stats.getStrength() + 5);
            stats.setDexterity(stats.getDexterity() + 2);
            stats.setIntelligence(stats.getIntelligence() + 1);
        }
    }
}
