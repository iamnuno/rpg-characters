package heroes;

import items.armor.Armor;
import items.weapons.Weapon;
import models.Equipment;
import models.Level;
import models.Stats;

/**
 * Abstract class from where new characters classes can be extended
 * to create different hero styles.
 */

public abstract class Hero {

    protected Stats stats;
    protected Stats bonusStats;
    protected Level level;
    protected Equipment equipment;

    public Hero(Stats stats, Level levels) {
        this.stats = stats;
        this.level = levels;
    }

    /**
     * Common method to all hero extended classes.
     * When increasing the xp it is necessary to call Level's increase xp method
     * to verify if the character leveled up, if so levelUp method is called with
     * the number of level ups to update each individual base stats.
     *
     * @param xp integer representing the number of xp to increase
     */
    public void increaseXp(int xp) {
        int levelUpCount = level.increaseXp(xp);
        if (levelUpCount > 0) {
            levelUp(levelUpCount);
        }
    }

    /**
     * Each class extending hero needs to have its own implementation
     * of leveling up because base stats are different from class to class.
     *
     * @param levelUpCount int representing how many levels the character upgraded
     */
    public abstract void levelUp(int levelUpCount);

    public void display() {
        System.out.println(stats + "\n" + level);
    }

    /**
     * Common method to all hero extended classes.
     * When equipping a character check all items and proceed with adding bonus stats
     * to base stats.
     * If the character's level is inferior to the item's level do not equip that item.
     *
     * @param equipment object holding items the character is equipping
     */
    public void equip(Equipment equipment) {

        Armor head = equipment.getHead();
        Armor body = equipment.getBody();
        Armor legs = equipment.getLegs();
        Weapon weapon = equipment.getWeapon();

        int dexterityBonus = 0;
        int hpBonus = 0;
        int intelligenceBonus = 0;
        int strengthBonus = 0;

        if (head != null) {
            int headLevel = head.getLevel();
            if (headLevel <= level.getLevel()) {
                dexterityBonus += head.getBonusStats().getDexterity();
                hpBonus += head.getBonusStats().getHp();
                intelligenceBonus += head.getBonusStats().getIntelligence();
                strengthBonus += head.getBonusStats().getStrength();
            } else {
                head = null;
                System.out.println("\nCould not equip head armor! Level up first!");
            }
        }

        if (body != null) {
            int bodyLevel = body.getLevel();
            if (bodyLevel <= level.getLevel()) {
                dexterityBonus += body.getBonusStats().getDexterity();
                hpBonus += body.getBonusStats().getHp();
                intelligenceBonus += body.getBonusStats().getIntelligence();
                strengthBonus += body.getBonusStats().getStrength();
            } else {
                body = null;
                System.out.println("\nCould not equip body armor! Level up first!");
            }
        }

        if (legs != null) {
            int legsLevel = legs.getLevel();
            if (legsLevel <= level.getLevel()) {
                dexterityBonus += legs.getBonusStats().getDexterity();
                hpBonus += legs.getBonusStats().getHp();
                intelligenceBonus += legs.getBonusStats().getIntelligence();
                strengthBonus += legs.getBonusStats().getStrength();
            } else {
                legs = null;
                System.out.println("\nCould not equip legs armor! Level up first!");
            }
        }

       if (weapon != null) {
            if (weapon.getLevel() > level.getLevel()) {
                System.out.println("\nCould not equip weapon! Level up first!");
                weapon = null;
            }
        }

        stats.setDexterity(stats.getDexterity() + dexterityBonus);
        stats.setIntelligence(stats.getIntelligence() + intelligenceBonus);
        stats.setHp(stats.getHp() + hpBonus);
        stats.setStrength(stats.getStrength() + strengthBonus);

        this.bonusStats = new Stats(hpBonus, strengthBonus, dexterityBonus, intelligenceBonus);
        this.equipment = new Equipment(weapon, head, body, legs);
    }

    /**
     * Common method to all hero extended classes.
     * When removing the equipment return to stats without bonus.
     */
    public void unEquip() {
        stats.setDexterity(stats.getDexterity() - bonusStats.getDexterity());
        stats.setIntelligence(stats.getIntelligence() - bonusStats.getIntelligence());
        stats.setHp(stats.getHp() - bonusStats.getHp());
        stats.setStrength(stats.getStrength() - bonusStats.getStrength());

        equipment = null;
    }

    /**
     * Common method to all hero extended classes.
     * When attacking check with type of weapon it is to apply correct modifiers.
     * At this point, hero stats already have its base + bonus calculated.
     */
    public void attack() {
        Weapon weapon = equipment.getWeapon();
        int damage;
        if (weapon != null) {
            String weaponType = weapon.getWeaponType();
            switch (weaponType) {
                case "Melee" -> {
                    damage = (int) (stats.getStrength() * 1.5 + weapon.getBaseDamage());
                    System.out.println("Attacking with " + damage + "...");
                }
                case "Range" -> {
                    damage = (int) (stats.getDexterity() * 2 + weapon.getBaseDamage());
                    System.out.println("Attacking with " + damage + "...");
                }
                case "Magic" -> {
                    damage = (int) (stats.getIntelligence() * 3 + weapon.getBaseDamage());
                    System.out.println("Attacking with " + damage + "...");
                }
            }
        } else {
            System.out.println("No damage!");
        }
    }
}
