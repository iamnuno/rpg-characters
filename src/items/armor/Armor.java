package items.armor;

import items.Item;
import models.Stats;
import models.Slot;

/**
 * Item's child class.
 * Each armor has its own style.
 */
public class Armor extends Item {
    private Stats bonusStats;
    private ArmorStyle armorStyle;
    private Slot slot;

    public Armor(String name, int level, ArmorStyle armorStyle, Slot slot) {
        super(name, level);
        this.armorStyle = armorStyle;
        bonusStats = new Stats(0,0,0,0);
        this.slot = slot;
    }

    /**
     * To update the armor's base stats it is necessary to call the armor's style own
     * update method as it is different from style to style.
     */
    public void updateBonusStats() {
        bonusStats = armorStyle.updateBonusStats(level);
    }

    /**
     * Bonus stats need to be scaled based on what type of slot they are on.
     */
    public void scaleBonusStats() {
        double mod = switch (slot.getSlotType()) {
            case HEAD -> 0.8;
            case LEGS -> 0.6;
            default -> 1;
        };

        bonusStats.setHp((int) (bonusStats.getHp() * mod));
        bonusStats.setStrength((int) (bonusStats.getStrength() * mod));
        bonusStats.setDexterity((int) (bonusStats.getDexterity() * mod));
        bonusStats.setIntelligence((int) (bonusStats.getIntelligence() * mod));
    }

    public void display() {
        System.out.println("\nStats for item: " + name + "\n");
        System.out.println("Type: " +  getArmorType() + " Armor");
        System.out.println("Slot: " +  getSlot().getSlotType());
        System.out.println("Level: " + level);
        System.out.println(bonusStats);
    }

    public String getArmorType() {
        return armorStyle.getType();
    }

    public Stats getBonusStats() {
        return bonusStats;
    }

    public void setBonusStats(Stats bonusStats) {
        this.bonusStats = bonusStats;
    }

    public void setArmorStyle(ArmorStyle armorStyle) {
        this.armorStyle = armorStyle;
    }

    public ArmorStyle getArmorStyle() {
        return armorStyle;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public Slot getSlot() {
        return slot;
    }
}
