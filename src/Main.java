import java.util.Scanner;

/**
 * Entry point for application.
 */
public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Game.mainMenu(input);
        input.close();
    }
}
