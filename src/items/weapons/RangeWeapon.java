package items.weapons;

/**
 * Type of weapon with specific own damage, scaled with item level.
 */
public class RangeWeapon implements WeaponStyle {

    @Override
    public int updateBaseDamage(int level) {
        return 5 + (level * 3);
    }

    @Override
    public String getType() {
        return "Range";
    }
}
