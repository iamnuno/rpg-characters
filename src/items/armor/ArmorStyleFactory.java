package items.armor;

/**
 * Factory used to return the required armor style.
 */
public class ArmorStyleFactory {
    public ArmorStyle getArmorStyle(ArmorType armorType) {
        return switch (armorType) {
            case CLOTH -> new ClothArmor();
            case PLATE -> new PlateArmor();
            case LEATHER -> new LeatherArmor();
        };
    }
}
