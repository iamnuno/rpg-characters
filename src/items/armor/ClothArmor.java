package items.armor;

import models.Stats;

/**
 * Type of armor with own specific stats.
 */
public class ClothArmor implements ArmorStyle {

    /**
     * Cloth armor has its own stats and level is used to scaled them.
     *
     * @param level int used to update stats value
     * @return stats object with calculated stats
     */
    @Override
    public Stats updateBonusStats(int level) {
        return new Stats(10 + (5 * level), 0, 1 + level, 3 + (2 * level));
    }

    @Override
    public String getType() {
        return "Cloth";
    }
}
