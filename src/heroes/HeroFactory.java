package heroes;

/**
 * Factory to return the desired hero type.
 */
public class HeroFactory {
    public Hero getHero(HeroTypes heroType) {
        return switch (heroType) {
            case MAGE -> new MageHero();
            case RANGER -> new RangerHero();
            case WARRIOR -> new WarriorHero();
        };
    }
}
