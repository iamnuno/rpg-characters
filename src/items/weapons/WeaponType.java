package items.weapons;

public enum WeaponType {
    MAGIC,
    MELEE,
    RANGE
}
